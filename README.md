简介
====
Qtc_CppCheck是一个Qt Creator IDE的插件，通过调用CppCheck程序检查代码文件，将检查结果直观地显示在IDE中，实现随时检查、快速定位和修改Bug的功能，目前只检测C++。（需要安装CppCheck程序，http://cppcheck.net/）

使用说明
========
下载
--------
下载bin\4.4.0目录下的CppCheck4.dll文件，这是一个已经编译好的插件动态库文件，适用于官网下载的Qt Creator （windows）4.4.0及其以后的版本程序，将CppCheck4.dll文件复制到 C:\Qt\qtcreator-4.4.0\lib\qtcreator\plugins 目录下，重新启动Qt Creator，即会自动加载该插件，在QtCreator菜单"Help"-"About Plugins..."中能够看到该插件
![](http://git.oschina.net/zhouliqiu/qtc_cppcheck/raw/master/screenshot/about.png)

选项
--------
在QtCreator菜单"Tools"-"Options..."中可以看到该插件的设置项，选择正确的CppCheck程序的路径。
![](http://git.oschina.net/zhouliqiu/qtc_cppcheck/raw/master/screenshot/option.png)

使用
--------
### 检查当前文件
打开一个源文件，使用快捷键"Alt+F9"，或者选择菜单"Tools"-"CppCheck检查"-"检查当前文件"
### 检查当前项目
选择菜单"Tools"-"CppCheck检查"-"检查当前项目"，弹出检查当前项目对话框，从下拉框中选择项目，勾选需要检查的文件，点击“开始检测”，注意：检测当前项目时，请保存所有当前打开的文档，否则当前修改不会被检查到。
![](http://git.oschina.net/zhouliqiu/qtc_cppcheck/raw/master/screenshot/project.png)
### 检查项目管理器
在项目管理器中，选择一个节点，点击右键，选择"使用CppCheck检查"

![](http://git.oschina.net/zhouliqiu/qtc_cppcheck/raw/master/screenshot/right-click.png)
### 检查结果
检查结果显示IDE的问题列表中
![](http://git.oschina.net/zhouliqiu/qtc_cppcheck/raw/master/screenshot/check-result.png)

自行编译
========
QtCreator插件的编译需要使用QtCreator的源代码和编译输出库，所以要先得到一个QtCreator 源码目录和一个编译输出目录，由于QtCreator代码里枚举变量经常改变,代码只对应当前QtCreator的最新版本

* 在http://download.qt-project.org/ 下载一个最新版本的QtCreator源代码

* 解压缩到一个目录中，比如D:\qt-creator-src-4.4.0

* 新建一个目录D:\qt-creator-build-4.4.0

* 运行QtCreator程序打开D:\qt-creator-src-4.4.0\QtCreator.pro

* 选择一个Qt版本的构建配置，Qt版本和编译器的选择很重要，插件的qt库版本和编译器版本必须要跟qtcreator.exe 一致才能被正确加载。

  比如现在官方网站下载的QtCreator安装程序都是使用Qt5.9.1+Vs2015（32位）编译出来的，那么编译插件时的构建配置也必须是Qt5.9.1+Vs2015（32 位）。

* 使用Shadow build，目录选择到D:\qt-creator-build-4.4.0

* 选择release编译，开始编译，大概需要30分钟。

* 运行QtCreator程序打开cppcheck.pro文件，选择跟编译QtCreator同版本的构建配置

* 找到第26行，把QtCREATOR_SOURCES=改成你的QtCreator源码目录 第31行，把IDE_BUILD_TREE=改成QtCreator的编译输出目录

* 选择release编译，开始编译，编译完成后，自动在QtCreator的编译输出目录里的lib/qtcreator/plugins/下生成动态库文件。