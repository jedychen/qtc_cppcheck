/****************************************************************************
** Copyright (C) 2017 ZhouLiQiu(zhouliqiu@126.com).
**
** This file is part of the qtc_cppcheck, It is a plugin for Qt Creator.
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in
** all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
** THE SOFTWARE.
**
****************************************************************************/
#include "cppcheckplugin.h"
#include "cppcheckconstants.h"

#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>

#include <coreplugin/idocument.h>
#include <coreplugin/editormanager/editormanager.h>

#include <projectexplorer/projectexplorer.h>
#include <projectexplorer/projectnodes.h>
#include <projectexplorer/project.h>
#include <projectexplorer/taskhub.h>
#include <projectexplorer/projecttree.h>

#include <QAction>
#include <QMessageBox>
#include <QMainWindow>
#include <QMenu>

#include "optionpage.h"
#include "csettings.h"
#include "cdlgcheckproject.h"
#include "worker_check.h"

namespace CppCheck {
namespace Internal {

CppCheckPlugin::CppCheckPlugin()
{
    // Create your members
    m_worker_check = new Worker_Check;
}

CppCheckPlugin::~CppCheckPlugin()
{
    // Unregister objects from the plugin manager's object pool
    // Delete members
    m_worker_check->stopCheck();
    delete m_worker_check;
}

bool CppCheckPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    // Register objects in the plugin manager's object pool
    // Load settings
    // Add actions to menus
    // Connect to other plugins' signals
    // In the initialize function, a plugin can be sure that the plugins it
    // depends on have initialized their members.
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    QAction *actionNode = new QAction(QString::fromLocal8Bit("使用CppCheck检查"), this);
    Core::Command *cmdNode = Core::ActionManager::registerAction(actionNode, Constants::ACTION_NODE_ID,
                                Core::Context(Core::Constants::C_EDIT_MODE));
    connect(actionNode, &QAction::triggered, this, &CppCheckPlugin::triggerActionNode);

    Core::ActionContainer *tempMenu = Core::ActionManager::actionContainer(ProjectExplorer::Constants::M_FILECONTEXT);
    if(tempMenu != NULL)
    {
        tempMenu->addAction(cmdNode);
    }
    tempMenu = Core::ActionManager::actionContainer(ProjectExplorer::Constants::M_FOLDERCONTEXT);
    if(tempMenu != NULL)
    {
        tempMenu->addAction(cmdNode);
    }
    tempMenu = Core::ActionManager::actionContainer(ProjectExplorer::Constants::M_PROJECTCONTEXT);
    if(tempMenu != NULL)
    {
        tempMenu->addAction(cmdNode);
    }
    tempMenu = Core::ActionManager::actionContainer(ProjectExplorer::Constants::M_SUBPROJECTCONTEXT);
    if(tempMenu != NULL)
    {
        tempMenu->addAction(cmdNode);
    }
    ProjectExplorer::TaskHub::addCategory(Constants::TASK_CATEGORY_ID,
                                          QString::fromLocal8Bit(Constants::TASK_CATEGORY_NAME));

    QAction *actionFile = new QAction(QString::fromLocal8Bit("检查当前文件"), this);
    Core::Command *cmdFile = Core::ActionManager::registerAction(actionFile, Constants::ACTION_FILE_ID,
                             Core::Context(Core::Constants::C_GLOBAL));
    cmdFile->setDefaultKeySequence(QKeySequence(("Alt+F9")));
    connect(actionFile, &QAction::triggered, this, &CppCheckPlugin::triggerActionFile);

    QAction *actionProject = new QAction(QString::fromLocal8Bit("检查当前项目"), this);
    Core::Command *cmdProject = Core::ActionManager::registerAction(actionProject, Constants::ACTION_PROJECT_ID,
                                Core::Context(Core::Constants::C_GLOBAL));
    connect(actionProject, &QAction::triggered, this, &CppCheckPlugin::triggerActionProject);

    Core::ActionContainer *menu = Core::ActionManager::createMenu(Constants::MENU_ID);
    menu->menu()->setTitle(QString::fromLocal8Bit("CppCheck检查"));
    menu->addAction(cmdFile);
    menu->addAction(cmdProject);
    Core::ActionManager::actionContainer(Core::Constants::M_TOOLS)->addMenu(menu);

    m_optionPage = new OptionPage;
    addAutoReleasedObject(m_optionPage);

    return true;
}

void CppCheckPlugin::extensionsInitialized()
{
    // Retrieve objects from the plugin manager's object pool
    // In the extensionsInitialized function, a plugin can be sure that all
    // plugins that depend on it are completely initialized.
}

ExtensionSystem::IPlugin::ShutdownFlag CppCheckPlugin::aboutToShutdown()
{
    // Save settings
    // Disconnect from signals that are not needed during shutdown
    // Hide UI (if you add UI that is not in the main window directly)
    return SynchronousShutdown;
}

void CppCheckPlugin::triggerActionNode()
{
    ProjectExplorer::Node* node = ProjectExplorer::ProjectTree::currentNode();
    if (node == NULL)
    {
        return;
    }
    m_worker_check->checkNode(node);
}

void CppCheckPlugin::triggerActionFile()
{
    Core::IDocument* document = Core::EditorManager::currentDocument();
    if (document == NULL)
    {
        return;
    }
    m_worker_check->checkFiles(QStringList() << document->filePath().toString());
}

void CppCheckPlugin::triggerActionProject()
{
    CDlgCheckProject dlg;
    dlg.exec();
}

} // namespace Internal
} // namespace CppCheck
