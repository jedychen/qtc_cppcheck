/****************************************************************************
** Copyright (C) 2017 ZhouLiQiu(zhouliqiu@126.com).
**
** This file is part of the qtc_cppcheck, It is a plugin for Qt Creator.
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in
** all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
** THE SOFTWARE.
**
****************************************************************************/
#pragma once

namespace CppCheck {
namespace Constants {

const char GLOBAL_ID[] = "CppCheck";

const char OPTIONS_ID[] = "CppCheck.Option";

const char ACTION_NODE_ID[] = "CppCheck.Action.Node";
const char ACTION_FILE_ID[] = "CppCheck.Action.File";
const char ACTION_PROJECT_ID[] = "CppCheck.Action.Project";
const char MENU_ID[] = "CppCheck.Menu";

const char SETTING_GROUP[] = "CppCheck.Setting.Group";
const char SETTING_EXE_FILENAME[] = "CppCheck.Setting.ExeFileName";
const char SETTING_POPUP_ERROR[] = "CppCheck.Setting.PopupError";
const char SETTING_POPUP_WARN[] = "CppCheck.Setting.PopupWarn";

const char TASK_CATEGORY_ID[] = "CppCheck.TaskCategory";
const char TASK_CATEGORY_NAME[] = "CppCheck���";

} // namespace CppCheck
} // namespace Constants
